import React, { Component } from 'react';
import Axios from 'axios';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

class ListNormal extends Component {
	state = {
		data:[],
		daftarnilai:[],
		nilaimax: [],
		kriteria:[],
		loading:false
	}
	all = async ()=>{
		let data =await Axios.get('https://spksaw.glitch.me/alternatif').then( (res) => { return res.data })
		let daftarnilai = await Promise.all(
			data.map(async a=>{
				return Axios.get(`https://spksaw.glitch.me/perhitungan/${a.id}/alternatif`).then( (res) => { return res.data })
			})
		)
		let nama_kriteria = await Axios.get('https://spksaw.glitch.me/kriteria').then( (res) => { return res.data })
		// console.log('bii', daftarnilai)
		this.setState({
			data:data,
			daftarnilai:daftarnilai,
			kriteria : nama_kriteria
		})
	}

	max = async ()=>{
		let datamax = await Axios.get('https://spksaw.glitch.me/perhitungan/max/').then( (res) => { return res.data })
		this.setState({
			nilaimax:datamax
		})
	}
	componentDidMount(){
		this.all();
		this.max();
	}


	hitung = (index)=>{
		let sum = 0;
		let numbers = this.state.daftarnilai[index]
		for (let i = 0; i < numbers.length; i++) {
			sum += ((numbers[i].nilai / this.state.nilaimax[i].nilai * numbers[i].bobot))
		}
		return sum.toFixed(2)
	}


    render() {
  	const { classes } = this.props;

	  return (
	    <React.Fragment>
		    <Paper className={classes.root}>
		      <Table className={classes.table}>
		        <TableHead>
		          <TableRow>
		            <TableCell rowSpan={2}>ID</TableCell>
		            <TableCell rowSpan={2}>Nama</TableCell>
		            <TableCell colSpan={4} style={{textAlign: 'center'}}>Kriteria</TableCell>
		          </TableRow>
		          <TableRow>
							{
								this.state.kriteria.map((aaa, index)=>{
									return <TableCell key={index}>{aaa.nama_kriteria}</TableCell>
								})
							}
		          </TableRow>
		        </TableHead>
		        <TableBody>
		          {this.state.data.map((datas, index) => {
		            return (
		              <TableRow key={datas.id}>
		                <TableCell component="th" scope="row">
		                  {datas.id}
		                </TableCell>
		                <TableCell >{datas.nama}</TableCell>
							{
								this.state.daftarnilai[index].map((aaa, index)=>{
									return <TableCell key={index}>{aaa.keterangan} </TableCell>
								})
							}
		              </TableRow>
		            );
		          })}
		        </TableBody>
		      </Table>
		    </Paper>
		    <br/>

		    <Paper className={classes.root}>
		      <Table className={classes.table}>
		        <TableHead>
		          <TableRow>
		            <TableCell rowSpan={2}>ID</TableCell>
		            <TableCell rowSpan={2}>Nama</TableCell>
		            <TableCell colSpan={4} style={{textAlign: 'center'}}>Kriteria</TableCell>
		          </TableRow>
		          <TableRow>
							{
								this.state.kriteria.map((aaa, index)=>{
									return <TableCell key={index}>{aaa.nama_kriteria}</TableCell>
								})
							}
		          </TableRow>
		        </TableHead>
		        <TableBody>
		          {this.state.data.map((datas, index) => {
		            return (
		              <TableRow key={datas.id}>
		                <TableCell component="th" scope="row">
		                  {datas.id}
		                </TableCell>
		                <TableCell >{datas.nama}</TableCell>
							{
								this.state.daftarnilai[index].map((aaa, index)=>{
									return <TableCell key={index}>{aaa.nilai} </TableCell>
								})
							}
		              </TableRow>
		            );
		          })}
		        </TableBody>
		      </Table>
		    </Paper>
		    <br/>

		    <Paper className={classes.root}>
		      <Table className={classes.table}>
		        <TableHead>
		          <TableRow>
		            <TableCell rowSpan={2}>ID</TableCell>
		            <TableCell rowSpan={2}>Nama</TableCell>
		            <TableCell colSpan={4} style={{textAlign: 'center'}}>Kriteria</TableCell>
		          </TableRow>
		          <TableRow>
							{
								this.state.kriteria.map((aaa, index)=>{
									return <TableCell key={index}>{aaa.nama_kriteria}</TableCell>
								})
							}
		          </TableRow>
		        </TableHead>
		        <TableBody>
		          {this.state.data.map((datas, index) => {
		            return (
		              <TableRow key={datas.id}>
		                <TableCell component="th" scope="row">
		                  {datas.id}
		                </TableCell>
		                <TableCell >{datas.nama}</TableCell>
							{
								this.state.daftarnilai[index].map((angka, index)=>{
									return (
											<TableCell key={index}>{(angka.nilai / this.state.nilaimax[index].nilai).toFixed(2)}</TableCell>
									)
								})
							}
		              </TableRow>
		            );
		          })}
		        </TableBody>
		      </Table>
		    </Paper>

		    <br/>
		    <Paper className={classes.root}>
		      <Table className={classes.table}>
		        <TableHead>
		          <TableRow>
		            <TableCell rowSpan={2}>ID</TableCell>
		            <TableCell rowSpan={2}>Nama</TableCell>
		            <TableCell colSpan={this.state.kriteria.length} style={{textAlign: 'center'}}>Kriteria</TableCell>
		            <TableCell rowSpan={2}>Total</TableCell>
		          </TableRow>
		          <TableRow>
							{
								this.state.kriteria.map((aaa, index)=>{
									return <TableCell key={index}>{aaa.nama_kriteria}</TableCell>
								})
							}
		          </TableRow>
		        </TableHead>
		        <TableBody>
		          {this.state.data.map((datas, index) => {
		            return (
		              <TableRow key={datas.id}>
		                <TableCell component="th" scope="row">
		                  {datas.id}
		                </TableCell>
		                <TableCell >{datas.nama}</TableCell>
							{
								this.state.daftarnilai[index].map((angka, index)=>{
									return (
										<TableCell key={index}>{(angka.nilai / this.state.nilaimax[index].nilai * angka.bobot).toFixed(2)}</TableCell>
									)
								})
							}
						<TableCell>
							{
								this.hitung(index)
							}
						</TableCell>
		              </TableRow>
		            );
		          })}
		        </TableBody>
		      </Table>
		    </Paper>
	    </React.Fragment>
	  );
    }
}

ListNormal.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListNormal);



