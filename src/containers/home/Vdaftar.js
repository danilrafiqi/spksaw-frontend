import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
// import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
// import FilledInput from '@material-ui/core/FilledInput';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
// import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Axios from 'axios'
import TextField from '@material-ui/core/TextField';
import TombolDaftar from './TombolDaftar'

const styles = theme => ({
  root: {
  	margin:'0 auto',
    // display: 'flex',
    // flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    width:'100%',
    // maxWidth: 480,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class SimpleSelect extends React.Component {
  state = {
    C1: [],
    C2: [],
    C3: [],
    C4: [],
    VC1: '',
    VC2: '',
    VC3: '',
    VC4: '',
    npm:''
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  createKriteria = () => {
    Axios
      .post('https://spksaw.glitch.me/perhitungan/', {
        id_alternatif:this.state.id_alternatif,
        id_nilai_tiap_kriteria:this.state.id_nilai_tiap_kriteria
      })
      .then(res => {
        this.setState({
          open: false,
          id_nilai_tiap_kriteria: null,
          id_alternatif:null
        });
        this.props.getData();
      });
  };

  all = async ()=>{
    let C1 = await Axios.get('https://spksaw.glitch.me/nilaikriteria/C1/nilai').then( (res) => { return res.data })
    let C2 = await Axios.get('https://spksaw.glitch.me/nilaikriteria/C2/nilai').then( (res) => { return res.data })
    let C3 = await Axios.get('https://spksaw.glitch.me/nilaikriteria/C3/nilai').then( (res) => { return res.data })
    let C4 = await Axios.get('https://spksaw.glitch.me/nilaikriteria/C4/nilai').then( (res) => { return res.data })
    this.setState({
      C1:C1,
      C2:C2,
      C3:C3,
      C4:C4
    })
  }
  componentDidMount(){
    this.all();
  }

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.root} autoComplete="off">
        <FormControl variant="outlined" className={classes.formControl}>
        <TextField
          id="outlined-name"
          label="NPM"
          className={classes.textField}
          value={this.state.npm}
          onChange={this.handleChange}
          margin="normal"
          variant="outlined"
          name='npm'
        />
        </FormControl>
        <br/>
        <br/>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel
            ref={ref => {
              this.labelRef = ReactDOM.findDOMNode(ref);
            }}
            htmlFor="C1"
          >
            IPK
          </InputLabel>
          <Select
            value={this.state.VC1}
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                name="VC1"
                id="C1"
              />
            }
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {
              this.state.C1.map((datas, index) => {
                return(
                  <MenuItem key={index} value={datas.id}>{datas.keterangan}</MenuItem>
                )
              })
            }
          </Select>
        </FormControl>
<br/>
<br/>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel
            ref={ref => {
              this.labelRef = ReactDOM.findDOMNode(ref);
            }}
            htmlFor="C2"
          >
            Semester
          </InputLabel>
          <Select
            value={this.state.VC2}
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                name="VC2"
                id="C2"
              />
            }
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {
              this.state.C2.map((datas, index) => {
                return(
                  <MenuItem key={index} value={datas.id}>{datas.keterangan}</MenuItem>
                )
              })
            }
          </Select>
        </FormControl>
<br/>
<br/>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel
            ref={ref => {
              this.labelRef = ReactDOM.findDOMNode(ref);
            }}
            htmlFor="C3"
          >
            Penghasilan Orang Tua
          </InputLabel>
          <Select
            value={this.state.VC3}
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                name="VC3"
                id="C3"
              />
            }
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {
              this.state.C3.map((datas, index) => {
                return(
                  <MenuItem key={index} value={datas.id}>{datas.keterangan}</MenuItem>
                )
              })
            }
          </Select>
        </FormControl>

<br/>
<br/>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel
            ref={ref => {
              this.labelRef = ReactDOM.findDOMNode(ref);
            }}
            htmlFor="C4"
          >
            Tanggungan Orang Tua
          </InputLabel>
          <Select
            value={this.state.VC4}
            onChange={this.handleChange}
            input={
              <OutlinedInput
                labelWidth={this.labelRef ? this.labelRef.offsetWidth : 0}
                name="VC4"
                id="C4"
              />
            }
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            {
              this.state.C4.map((datas, index) => {
                return(
                  <MenuItem key={index} value={datas.id}>{datas.keterangan}</MenuItem>
                )
              })
            }
          </Select>
<br/>
<br/>

          <TombolDaftar/>

        </FormControl>
      </form>

    );
  }
}

SimpleSelect.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleSelect);