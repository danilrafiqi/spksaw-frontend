import Home from './Home'
import Daftar from './Daftar'
import Hasil from './Hasil'
import Kartu from './Kartu'
import Pengumuman from './Pengumuman'

export {
	Home,
	Daftar,
	Hasil,
	Kartu,
	Pengumuman
}