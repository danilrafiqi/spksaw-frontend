import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {Link} from 'react-router-dom'
import Kartu from './Kartu'

const styles = theme => ({
  flex: {
    flexGrow: 1,
  },
  cleanLink:{
    textDecoration :'none',
    color:'white'
  },
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundSize: 'cover',
    backgroundImage: "url('http://www.polinela.ac.id/templates/jb-polinela2016-25/images/polinela_background_2017.jpg')"
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit * 6,
  },
});


function Home(props) {
  const { classes } = props;

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="static" className={classes.appBar}>
        <Toolbar>
          <HomeIcon className={classes.icon} />
          <Typography className={classes.flex} variant="title" color="inherit" noWrap>
            SPK Beasiswa
          </Typography>
          <Link to='/pengumuman' className={classes.cleanLink}>
            <Button color="inherit">Hasil Seleksi</Button>
          </Link>
        </Toolbar>
      </AppBar>
      <main>
        {/* Hero unit */}
        <div className={classes.heroUnit}>
          <div className={classes.heroContent}>
            <Typography variant="display3" align="center" color="textPrimary" gutterBottom>
              SPK Beasiswa
            </Typography>
            <Typography variant="title" align="center" color="textSecondary" paragraph>
              SPK Beasiswa merupakan aplikasi untuk melakukan penyeleksian siapa mahasiswa yang berhak untuk mendapatkan beasiswa
            </Typography>

          <Link to='/daftar' className={classes.cleanLink}>
            <div className={classes.heroButtons}>
              <Grid container spacing={16} justify="center">
                <Grid item>
                  <Button variant="contained" color="primary">
                    Mulai Gunakan
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Link>

          </div>
        </div>
        <Kartu/>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant="title" align="center" gutterBottom>
          SPKBEASISWA
        </Typography>
        <Typography variant="subheading" align="center" color="textSecondary" component="p">
          Mi Polinela
        </Typography>
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);