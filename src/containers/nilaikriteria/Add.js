import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

//import inputan
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';
import Axios from 'axios';


function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
//style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },


  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },

});

class Add extends React.Component {
  state = {
    open: false,
    id: null,
    keterangan:null,
    nilai:null,
    id_kriteria:null
  };

 dataForm = [
    {
      title:"Id",
      name:"id",
      nilai:this.state.id
    },
    {
      title:"Keterangan",
      name:"keterangan",
      nilai:this.state.keterangan
    },
    {
      title:"Nilai",
      name:"nilai",
      nilai:this.state.nilai
    },
    {
      title:"Id Kriteria",
      name:"id_kriteria",
      nilai:this.state.id_kriteria
    }
  ];


  handleChange = e => {
    this.setState({
    	[e.target.name]: e.target.value
    });
  };


  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  createKriteria = () => {
    Axios
      .post('https://spksaw.glitch.me/nilaikriteria/', {
        id:this.state.id,
        keterangan:this.state.keterangan,
        nilai:this.state.nilai,
        id_kriteria:this.state.id_kriteria
      })
      .then(res => {
        this.setState({
          open: false,
          id:null,
          nama_kriteria:null,
          bobot:null
        });
        this.props.getData();
      });
  };

  render() {
    const { classes } = this.props;

    return (
      <div>
      <Button onClick={this.handleOpen} className={classes.button} variant="contained" color="secondary" >
        <AddIcon  />
      </Button>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Add Data
            </Typography>
            <br/>
            <Typography variant="subheading" id="simple-modal-description">
    		      <div className={classes.container}>
                {
                  this.dataForm.map( (datas, index) => {
                    return(
          		        <FormControl className={classes.formControl} key={index}>
          		          <InputLabel htmlFor="">{datas.title}</InputLabel>
          		          <Input id="" name={datas.name} value={datas.value} fullWidth={true} onChange={this.handleChange} />
                      </FormControl>
                    )
                  })
                }
    		      </div>
            </Typography>

      <Button onClick={this.createKriteria} className={classes.button} variant="contained" color="secondary" >
        Save
        <SaveIcon className={classes.rightIcon}  />
      </Button>

          </div>
        </Modal>
      </div>
    );
  }
}

Add.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Add);