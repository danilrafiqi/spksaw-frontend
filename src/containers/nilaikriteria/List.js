import React, { Component } from 'react';
import Axios from 'axios';


import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


import Add from './Add'
import Update from './Update'

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

class List extends Component {
	state = {
		data:[],
		loading:false
	}
	all = () => {
		Axios
			.get('https://spksaw.glitch.me/nilaikriteria')
			.then(res =>{
				this.setState({
					data:res.data,
					loading:true
				})
			})
	}

	delete = (id) => {
		Axios
			.delete(`https://spksaw.glitch.me/nilaikriteria/${id}`)
			.then(res =>{
				this.all()
			})
	}

	componentDidMount(){
		this.all();
	}


    render() {
  	const { classes } = this.props;

	  return (
	    <Paper className={classes.root}>
	    {
	    	//buat tombol add
	    }
	    <Add getData={this.all}/>
	      <Table className={classes.table}>
	        <TableHead>
	          <TableRow>
	            <TableCell>ID</TableCell>
	            <TableCell >Keterangan</TableCell>
	            <TableCell >Nilai</TableCell>
	            <TableCell >Nama Kriteria</TableCell>
	            <TableCell >Action</TableCell>
	          </TableRow>
	        </TableHead>

	        <TableBody>
	          {this.state.data.map((datas) => {
	            return (
	              <TableRow key={datas.id}>
	                <TableCell component="th" scope="row">
	                  {datas.id}
	                </TableCell>
	                <TableCell >{datas.keterangan}</TableCell>
	                <TableCell >{datas.nilai}</TableCell>
	                <TableCell >{datas.nama_kriteria}</TableCell>
	                <TableCell >
				      <Button
					      onClick={
					      	()=>{
					      		this.delete(datas.id)
					      	}
					      }
					      variant="contained" size="small" color="secondary" >
				        <DeleteIcon/>
				      </Button>
	                	<Update getData={this.all} detail={datas.id}/>
	                </TableCell>
	              </TableRow>
	            );
	          })}
	        </TableBody>
	      </Table>
	    </Paper>
	  );
    }
}

List.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(List);