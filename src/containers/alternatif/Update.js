import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import Button from '@material-ui/core/Button';

//import inputan
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';

import EditIcon from '@material-ui/icons/Edit';
import UpdateIcon from '@material-ui/icons/Update';
import Axios from 'axios';
import Aux from '../../hoc/Auxiliary'

function getModalStyle() {
  // const top = 50 + rand();
  // const left = 50 + rand();
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = theme => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
//style inputan
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
  },


  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },

});

class Update extends React.Component {
  state = {
    open: false,
    id:null,
    nama:null
  };

  detail = (id) => {
    Axios
      .get(`https://spksaw.glitch.me/alternatif/${id}`, {
      })
      .then(res => {
        this.setState({
          id:res.data[0].id,
          nama:res.data[0].nama,
	        open: true
        });
      });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };


  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };



  update = (id) => {
    Axios
      .put(`https://spksaw.glitch.me/alternatif/${id}`, {
        id:this.state.id,
        nama:this.state.nama,
      })
      .then(res => {
        this.setState({
          open: false,
          id:null,
          nama:null
        });
        this.props.getData();
      });
  };



  render() {
    const { classes } = this.props;

    return (
      <Aux>
      <Button onClick={()=>{this.detail(this.props.detail)}} className={classes.button} variant="contained" color="secondary" >
        <EditIcon  />
      </Button>

        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <div style={getModalStyle()} className={classes.paper}>
            <Typography variant="title" id="modal-title">
              Update Data
            </Typography>
            <br/>
            <Typography variant="subheading" id="simple-modal-description">






		      <div className={classes.container}>
		        <FormControl className={classes.formControl}>
		          <InputLabel htmlFor="">ID</InputLabel>
		          <Input id="" name='id' value={this.state.id} fullWidth={true} onChange={this.handleChange} />
		        </FormControl>

		        <FormControl className={classes.formControl}>
		          <InputLabel htmlFor="">Nama Kriteria</InputLabel>
		          <Input id="" name='nama' value={this.state.nama} fullWidth={true} onChange={this.handleChange} />
		        </FormControl>

		      </div>


{
	// onClick={()=>{this.updateRestaurant(this.props.detail)}}
}
            </Typography>

      <Button onClick={()=>{
        	this.update(this.props.detail)
        }} className={classes.button} variant="contained" color="secondary" >
        Update
        <UpdateIcon  className={classes.rightIcon}  />
      </Button>

          </div>
        </Modal>
      </Aux>
    );
  }
}

Update.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Update);