import {
    List as CriteriaList,
    Add as CriteriaAdd,
    Update as CriteriaUpdate
} from './criteria';

import {
    List as NilaiKriteriaList,
    Add as NilaiKriteriaAdd,
    Update as NilaiKriteriaUpdate
} from './nilaikriteria';

import {
    List as AlternatifList,
    Add as AlternatifAdd,
    Update as AlternatifUpdate
} from './alternatif';

import {
    List as PerhitunganList,
    ListNormal as PerhitunganListNormal,
    Add as PerhitunganAdd,
    Update as PerhitunganUpdate
} from './perhitungan';

import {
  SignIn
} from './auth';

import {
  Home, Daftar, Hasil,Pengumuman
} from './home';

export {
  Home,
  Daftar,
  Hasil,
  SignIn,
  Pengumuman,
  CriteriaList,
  CriteriaAdd,
  CriteriaUpdate,
  NilaiKriteriaList,
  NilaiKriteriaAdd,
  NilaiKriteriaUpdate,
  AlternatifList,
  AlternatifAdd,
  AlternatifUpdate,
  PerhitunganList,
  PerhitunganListNormal,
  PerhitunganAdd,
  PerhitunganUpdate
}