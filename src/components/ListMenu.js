import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import DescriptionIcon from '@material-ui/icons/Description';
import ListAltIcon from '@material-ui/icons/ListAlt';
import AssignmentIcon from '@material-ui/icons/Assignment';
import {Link} from 'react-router-dom';


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  cleanLink:{
    textDecoration :'none'
  }
});

const menu = [
  {
    link:'/kriteria',
    icon:<DescriptionIcon/>,
    text:'Kriteria'
  },
  {
    link:'/nilaikriteria',
    icon:<ListAltIcon/>,
    text:'Nilai Kriteria'
  },
  {
    link:'/alternatif',
    icon:<PeopleIcon/>,
    text:'Alternatif'
  },
  {
    link:'/perhitungan',
    icon:<DashboardIcon/>,
    text:'Perhitungan'
  },
  {
    link:'/perhitungan/laporan',
    icon:<AssignmentIcon/>,
    text:'Laporan'
  }
]

function ListMenu(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <Divider />
      <List component="nav">
      {
        menu.map( (datas, index) => {
          return(
            <Link to={datas.link} className={classes.cleanLink} key={index}>
              <ListItem button>
                <ListItemIcon>
                  {datas.icon}
                </ListItemIcon>
                <ListItemText primary={datas.text} />
              </ListItem>
            </Link>
          )
        })
      }
      </List>
    </div>
  );
}

ListMenu.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListMenu);
