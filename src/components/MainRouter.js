import React, { Component } from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import {Switch, Route} from 'react-router-dom';
import {PerhitunganList, PerhitunganListNormal, AlternatifList, NilaiKriteriaList, CriteriaList} from '../containers'

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
});

class MainRouter extends Component {
	render() {
		const { classes } = this.props;
		return (
          <main className={classes.content}>

            <div className={classes.appBarSpacer} />
	          <Typography variant="display1" noWrap>
		        <Switch>
		            <Route exact={true} path="/" component={AlternatifList}/>
		        </Switch>

		        <Switch>
		            <Route path="/nilaikriteria" component={NilaiKriteriaList}/>
		            <Route path="/kriteria" component={CriteriaList }/>
		            <Route path="/alternatif" component={AlternatifList }/>
		            <Route path="/perhitungan/laporan" component={PerhitunganListNormal }/>
		            <Route path="/perhitungan" component={PerhitunganList }/>
		            <Route path="/dashboard" component={CriteriaList }/>
		        </Switch>
	          </Typography>
          </main>
		);
	}
}

export default  withStyles(styles)(MainRouter);