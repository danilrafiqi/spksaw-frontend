import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import Dashboard from './components/Dashboard';
import Authenticated from './components/Authenticated';

import {Switch, Route} from 'react-router-dom';
import {SignIn, Home, Daftar, Pengumuman} from './containers'

class App extends Component {
  render() {
    return (
      <Switch>
            <Route path="/signin" component={SignIn}/>
            <Route path="/daftar" component={Daftar}/>
            <Route path="/pengumuman" component={Pengumuman}/>
            <Route path="/" exact component={Home}/>
			<Authenticated>
				<Route path="/" component={Dashboard} />
			</Authenticated>
      </Switch>
    );
  }
}

export default App;
